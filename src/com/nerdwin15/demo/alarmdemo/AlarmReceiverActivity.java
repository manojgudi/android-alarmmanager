package com.nerdwin15.demo.alarmdemo;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.telephony.TelephonyManager;

/**
 * This activity will be called when the alarm is triggered.
 * 
 * @author Michael Irwin
 */
public class AlarmReceiverActivity extends Activity {
	public double[] location;
	public String IMEI, Phone;
	public String Hash;

	public String ServerURL = "http://outsideloc.appspot.com/";

	LocFunctions LocFunction = new LocFunctions();
	Posting http_post = new Posting();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Get Location
		LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		location = LocFunction.getGPS(lm);

		// playSound(this, getAlarmUri());
		TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

		IMEI = telephonyManager.getDeviceId();

		Phone = telephonyManager.getSimSerialNumber();

		TriggeredOnAlarm(IMEI, Phone, location);

		finish();
	}

	@SuppressLint("NewApi")
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	private void TriggeredOnAlarm(String IMEIObj, String PhoneObj,
			double[] locationObj) {
		// Get Location

		// Compute Hash

		try {
			Hash = LocFunction.computeHash(IMEIObj + PhoneObj);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		// Post data
		http_post.postData(ServerURL, Hash, locationObj[0], locationObj[1]);
	}

}